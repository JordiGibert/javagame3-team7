package visible;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import javax.swing.JRadioButton;
import java.awt.Font;
import java.awt.Window;

import javax.swing.SwingConstants;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VisibleDificultad extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	static VisibleDificultad frame = new VisibleDificultad();
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VisibleDificultad() {
		setTitle("Nivel de difficultad");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 237, 236);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(60, 22, 122, 129);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JRadioButton rdbtnFacil = new JRadioButton("Principiante");
		rdbtnFacil.setSelected(true);
		rdbtnFacil.setBounds(6, 7, 109, 23);
		panel.add(rdbtnFacil);
		
		JRadioButton rdbtnMedio = new JRadioButton("Medio");
		rdbtnMedio.setBounds(6, 51, 109, 23);
		panel.add(rdbtnMedio);
		
		JRadioButton rdbtnDificil = new JRadioButton("Avanzado");
		rdbtnDificil.setBounds(6, 99, 109, 23);
		panel.add(rdbtnDificil);
		
		ButtonGroup dificultad = new ButtonGroup();
		dificultad.add(rdbtnFacil);
		dificultad.add(rdbtnMedio);
		dificultad.add(rdbtnDificil);
		
		JButton btnIniciar = new JButton("Iniciar");
		btnIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int dificultad=1;
				if (rdbtnMedio.isSelected()) {
					dificultad=2;
				}else if(rdbtnDificil.isSelected()) {
					dificultad=3;
				}
				VisibleMain joc= new VisibleMain(dificultad);
				joc.setLocationRelativeTo(null);
			    frame.setVisible(false);
			    joc.setVisible(true);
			}
		});
		btnIniciar.setBounds(10, 162, 89, 23);
		contentPane.add(btnIniciar);
		
		JButton btnSalir = new JButton("Cancelar");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnSalir.setBounds(132, 162, 89, 23);
		contentPane.add(btnSalir);
	}
}
