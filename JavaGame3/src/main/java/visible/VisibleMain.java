package visible;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

public class VisibleMain extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	/*public void ejecutar(int dificultad) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VisibleMain frame = new VisibleMain(dificultad);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	*/

	/**
	 * Create the frame.
	 */
	
	ArrayList<Integer> solucio = new ArrayList<Integer>();
	ArrayList<Integer> cadena = new ArrayList<Integer>();
	ArrayList<Integer> cadenaColores = new ArrayList<Integer>();

	int asiertos = 0;
	int postX = 10;
	int postY = 8;
	int postButtonY = 20; 
	int panelColor = 11;
	int aumentarPanel = 1;
	
	int[] colors;
	int numColors=0;
	int torn=0;
	int[] numsPista=new int[4];
	boolean finalizado=false;
	
	public VisibleMain(int dificultad) {
		setResizable(false);
		setEnabled(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 727, 545);
		switch (dificultad) {
		case 1:
			setBounds(100, 100, 650, 551);
			numColors=4;
			break;
		case 2:
			setBounds(100, 100, 650, 446);
			numColors=5;
			break;
		case 3:
			setBounds(100, 100, 650, 347);
			numColors=6;
			break;
	}
		
		colors = new int[numColors];
		contentPane = new JPanel();
		contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		colors=elegirColors(numColors);
		
		JPanel infoColors = new JPanel();
		infoColors.setBorder(new LineBorder(new Color(0, 0, 0)));
		infoColors.setLayout(null);
		infoColors.setBounds(420, 11, 217, 59);
		contentPane.add(infoColors);
		
		JButton infoCol_0 = new JButton("");
		infoCol_0.setEnabled(false);
		infoCol_0.setToolTipText("1");
		int nColor0 = colors[0];	
		infoCol_0=posarColor(infoCol_0, nColor0);
		cadenaColores.add(nColor0);
		infoCol_0.setBounds(10, 26, 24, 24);
		infoColors.add(infoCol_0);
		
		JButton infoCol_1 = new JButton("");
		infoCol_1.setEnabled(false);
		infoCol_1.setToolTipText("1");
		int nColor1 = colors[1];	
		infoCol_1=posarColor(infoCol_1, nColor1);
		cadenaColores.add(nColor1);
		infoCol_1.setBounds(44, 26, 24, 24);
		infoColors.add(infoCol_1);
		
		JButton infoCol_2 = new JButton("");
		infoCol_2.setEnabled(false);
		infoCol_2.setToolTipText("1");
		int nColor2 = colors[2];	
		infoCol_2=posarColor(infoCol_2, nColor2);
		cadenaColores.add(nColor2);
		infoCol_2.setBounds(78, 26, 24, 24);
		infoColors.add(infoCol_2);
		
		JButton infoCol_3 = new JButton("");
		infoCol_3.setEnabled(false);
		infoCol_3.setToolTipText("1");
		int nColor3 = colors[3];	
		infoCol_3=posarColor(infoCol_3, nColor3);
		cadenaColores.add(nColor3);
		infoCol_3.setBounds(112, 26, 24, 24);
		infoColors.add(infoCol_3);
		
		JButton infoCol_4 = new JButton("");
		infoCol_4.setEnabled(false);
		infoCol_4.setToolTipText("1");
		
		if(dificultad==1) {
			infoCol_4.setVisible(false);
		} else {
			int nColor4 = colors[4];	
			infoCol_4=posarColor(infoCol_4, nColor4);
			cadenaColores.add(nColor4);
		}
		
		infoCol_4.setBounds(146, 26, 24, 24);
		infoColors.add(infoCol_4);
		
		JButton infoCol_5 = new JButton("");
		infoCol_5.setEnabled(false);
		infoCol_5.setToolTipText("1");
		if(dificultad==3) {
			infoCol_5=posarColor(infoCol_5, colors[5]);
		} else {
			infoCol_5.setVisible(false);
		}
		infoCol_5.setBounds(180, 26, 24, 24);
		infoColors.add(infoCol_5);
		
		JPanel torn_secret = new JPanel();
		torn_secret.setLayout(null);
		torn_secret.setBounds(417, 133, 145, 40);
		//torn_secret.setVisible(false);
		
		JButton[] btonColores = new JButton[100];
		JButton[] btonAdv = new JButton[100];
		JButton[] btonSecret = new JButton[100];
		
		JPanel[] panelColores= new JPanel[100];

		 Random rand = new Random();
		 
		int secretX = 10;
		for (int i = 0; i < 4; i++) {
			contentPane.add(torn_secret);
			btonSecret[i] = new JButton("");
			btonSecret[i].setEnabled(false);
			btonSecret[i]=posarColor(btonSecret[i], cadenaColores.get(rand.nextInt(cadenaColores.size())));
			btonSecret[i].setBounds(secretX, 8, 24, 24);
			torn_secret.add(btonSecret[i]);	
			secretX+=34;
		}
		for (int s = 0; s < 1; s++) {
		
			panelColores[s] = new JPanel();
			panelColores[s].setLayout(null);
			panelColores[s].setBounds(10, panelColor, 145, 40);
			//torn1.setVisible(false);
			
			JPanel torn_info = new JPanel();
			torn_info.setLayout(null);
			torn_info.setBounds(275, panelColor, 145, 40);
			torn_info.setVisible(false);
			
			for (int i = 0; i < 4; i++) {		
				contentPane.add(panelColores[s]);						
				btonColores[i] = new JButton("");
				btonColores[i].setBackground(Color.WHITE);
				final Integer c = new Integer(i);
				btonColores[i].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {		
						btonColores[c]=canviarColor(btonColores[c]);
					}
				});
				
				btonColores[i].setToolTipText("1");
				btonColores[i].setBounds(postX, postY, 24, 24);
				panelColores[s].add(btonColores[i]);
				
				contentPane.add(torn_info);
				btonAdv[i] = new JButton("");
				btonAdv[i].setEnabled(false);
				btonAdv[i].setBackground(Color.WHITE);
				btonAdv[i].setBounds(postX, postY, 24, 24);
				torn_info.add(btonAdv[i]);			
				postX+=34;
			}		
			
			final Integer x = new Integer(s);
			
			JButton btnCompr = new JButton("Compr");
			btnCompr.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {		
					for (int i = 0; i < 4; i++) {					
						if(btonSecret[i].getBackground().equals(btonColores[i].getBackground())) {
							asiertos++;
						}			
					}
					
					for (int i = 0; i < asiertos; i++) {
						btonAdv[i].setBackground(Color.BLACK);
					}
					
					panelColores[x].setEnabled(false);
					btnCompr.setVisible(false);
					torn_info.setVisible(true);
					if(!btnCompr.isVisible()) {
						postButtonY+=30;
						
					}
				}			
			});
			
			btnCompr.setBounds(165, postButtonY, 82, 23);
			contentPane.add(btnCompr);
		}
		
		
		JLabel lblSecret = new JLabel("Colores disponibles:");
		lblSecret.setForeground(Color.BLUE);
		lblSecret.setBounds(45, 1, 115, 14);
		infoColors.add(lblSecret);
		
		
		JLabel lblNewLabel = new JLabel("Combinacion secreta:");
		lblNewLabel.setBounds(420, 120, 138, 14);
		contentPane.add(lblNewLabel);
		lblNewLabel.setForeground(Color.BLUE);
		
	}
		
//------------------------------------------LOGICA---------------------------------------
	
	// Generar serie de 4 num aleatoris per als colors
	int[] elegirColors(int numColor) {
		int [] colorsR = new int[numColor];
		colorsR[0]=1;
		/*colorsR[1]=2;
		colorsR[2]=3;
		colorsR[3]=4;*/
		for(int i =1; i<numColor; i++) {
			Random random= new Random();
			boolean igual=false;
			int color;
			do {
				igual=false;
				color =random.nextInt(10)+2;
				for(int o=1;o<numColor;o++) {
					if(color==colorsR[o]) {
						igual =true;
					}
				}
			} while(igual==true);
			colorsR[i]=color;
		}
		return colorsR;
	}
	
	//Canviar el color del boton
	JButton canviarColor(JButton btn) {
		int numColorBtn =Integer.parseInt(btn.getToolTipText());
		boolean colorTrobat=false;
		while(colorTrobat==false) {
			if(numColorBtn==11) {
				numColorBtn=0;
			} else {
				numColorBtn++;
			}
			for(int i=0;i<numColors;i++) {
				if(numColorBtn==colors[i] && colorTrobat==false) {
					colorTrobat=true;
				}
			}
		}
		posarColor(btn, numColorBtn);
		return btn;
	}
	
	JButton posarColor(JButton btn, int color) {
		switch(color) {
			case 1:
				btn.setBackground(Color.WHITE);
				btn.setToolTipText("1");
				break;
			case 2:
				btn.setBackground(Color.GRAY);
				btn.setToolTipText("2");
				break;
			case 3:
				btn.setBackground(Color.BLACK);
				btn.setToolTipText("3");
				break;
			case 4:
				btn.setBackground(Color.PINK);
				btn.setToolTipText("4");
				break;
			case 5:
				btn.setBackground(Color.YELLOW);
				btn.setToolTipText("5");
				break;
			case 6:
				btn.setBackground(Color.MAGENTA);
				btn.setToolTipText("6");
				break;
			case 7:
				btn.setBackground(Color.BLUE);
				btn.setToolTipText("7");
				break;
			case 8:
				btn.setBackground(Color.RED);
				btn.setToolTipText("8");
				break;
			case 9:
				btn.setBackground(Color.ORANGE);
				btn.setToolTipText("9");
				break;
			case 10:
				btn.setBackground(Color.GREEN);
				btn.setToolTipText("10");
				break;
			case 11:
				btn.setBackground(Color.CYAN);
				btn.setToolTipText("11");
				break;
		}
		return btn;
	}
}
